// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATargetPoint;
#ifdef BELLERNICOLE_AIEnemies_generated_h
#error "AIEnemies.generated.h already included, missing '#pragma once' in AIEnemies.h"
#endif
#define BELLERNICOLE_AIEnemies_generated_h

#define BellerNicole_Source_BellerNicole_AIEnemies_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGoToRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoToRandomWaypoint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ATargetPoint**)Z_Param__Result=P_THIS->GetRandomWaypoint(); \
		P_NATIVE_END; \
	}


#define BellerNicole_Source_BellerNicole_AIEnemies_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGoToRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GoToRandomWaypoint(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRandomWaypoint) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ATargetPoint**)Z_Param__Result=P_THIS->GetRandomWaypoint(); \
		P_NATIVE_END; \
	}


#define BellerNicole_Source_BellerNicole_AIEnemies_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAIEnemies(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AAIEnemies(); \
public: \
	DECLARE_CLASS(AAIEnemies, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AAIEnemies) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BellerNicole_Source_BellerNicole_AIEnemies_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAAIEnemies(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AAIEnemies(); \
public: \
	DECLARE_CLASS(AAIEnemies, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AAIEnemies) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BellerNicole_Source_BellerNicole_AIEnemies_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAIEnemies(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAIEnemies) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAIEnemies); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAIEnemies); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAIEnemies(AAIEnemies&&); \
	NO_API AAIEnemies(const AAIEnemies&); \
public:


#define BellerNicole_Source_BellerNicole_AIEnemies_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAIEnemies(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAIEnemies(AAIEnemies&&); \
	NO_API AAIEnemies(const AAIEnemies&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAIEnemies); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAIEnemies); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAIEnemies)


#define BellerNicole_Source_BellerNicole_AIEnemies_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Waypoints() { return STRUCT_OFFSET(AAIEnemies, Waypoints); }


#define BellerNicole_Source_BellerNicole_AIEnemies_h_14_PROLOG
#define BellerNicole_Source_BellerNicole_AIEnemies_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BellerNicole_Source_BellerNicole_AIEnemies_h_17_PRIVATE_PROPERTY_OFFSET \
	BellerNicole_Source_BellerNicole_AIEnemies_h_17_RPC_WRAPPERS \
	BellerNicole_Source_BellerNicole_AIEnemies_h_17_INCLASS \
	BellerNicole_Source_BellerNicole_AIEnemies_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BellerNicole_Source_BellerNicole_AIEnemies_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BellerNicole_Source_BellerNicole_AIEnemies_h_17_PRIVATE_PROPERTY_OFFSET \
	BellerNicole_Source_BellerNicole_AIEnemies_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	BellerNicole_Source_BellerNicole_AIEnemies_h_17_INCLASS_NO_PURE_DECLS \
	BellerNicole_Source_BellerNicole_AIEnemies_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BellerNicole_Source_BellerNicole_AIEnemies_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
