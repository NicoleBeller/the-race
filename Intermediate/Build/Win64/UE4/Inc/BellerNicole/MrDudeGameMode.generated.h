// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UUserWidget;
#ifdef BELLERNICOLE_MrDudeGameMode_generated_h
#error "MrDudeGameMode.generated.h already included, missing '#pragma once' in MrDudeGameMode.h"
#endif
#define BELLERNICOLE_MrDudeGameMode_generated_h

#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execChangeMenuWidget) \
	{ \
		P_GET_OBJECT(UClass,Z_Param_NewWidgetClass); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ChangeMenuWidget(Z_Param_NewWidgetClass); \
		P_NATIVE_END; \
	}


#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execChangeMenuWidget) \
	{ \
		P_GET_OBJECT(UClass,Z_Param_NewWidgetClass); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ChangeMenuWidget(Z_Param_NewWidgetClass); \
		P_NATIVE_END; \
	}


#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMrDudeGameMode(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AMrDudeGameMode(); \
public: \
	DECLARE_CLASS(AMrDudeGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AMrDudeGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMrDudeGameMode(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AMrDudeGameMode(); \
public: \
	DECLARE_CLASS(AMrDudeGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AMrDudeGameMode) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMrDudeGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMrDudeGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMrDudeGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMrDudeGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMrDudeGameMode(AMrDudeGameMode&&); \
	NO_API AMrDudeGameMode(const AMrDudeGameMode&); \
public:


#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMrDudeGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMrDudeGameMode(AMrDudeGameMode&&); \
	NO_API AMrDudeGameMode(const AMrDudeGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMrDudeGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMrDudeGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMrDudeGameMode)


#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StartingWidget() { return STRUCT_OFFSET(AMrDudeGameMode, StartingWidget); } \
	FORCEINLINE static uint32 __PPO__CurrentWidget() { return STRUCT_OFFSET(AMrDudeGameMode, CurrentWidget); } \
	FORCEINLINE static uint32 __PPO__PlayerHUDClass() { return STRUCT_OFFSET(AMrDudeGameMode, PlayerHUDClass); }


#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_11_PROLOG
#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_PRIVATE_PROPERTY_OFFSET \
	BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_RPC_WRAPPERS \
	BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_INCLASS \
	BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_PRIVATE_PROPERTY_OFFSET \
	BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_INCLASS_NO_PURE_DECLS \
	BellerNicole_Source_BellerNicole_MrDudeGameMode_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BellerNicole_Source_BellerNicole_MrDudeGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
