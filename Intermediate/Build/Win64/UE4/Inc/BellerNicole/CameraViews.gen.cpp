// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "CameraViews.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraViews() {}
// Cross Module References
	BELLERNICOLE_API UClass* Z_Construct_UClass_ACameraViews_NoRegister();
	BELLERNICOLE_API UClass* Z_Construct_UClass_ACameraViews();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_BellerNicole();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void ACameraViews::StaticRegisterNativesACameraViews()
	{
	}
	UClass* Z_Construct_UClass_ACameraViews_NoRegister()
	{
		return ACameraViews::StaticClass();
	}
	UClass* Z_Construct_UClass_ACameraViews()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_BellerNicole,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "CameraViews.h" },
				{ "ModuleRelativePath", "CameraViews.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HallwayCamera_MetaData[] = {
				{ "Category", "CameraViews" },
				{ "ModuleRelativePath", "CameraViews.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HallwayCamera = { UE4CodeGen_Private::EPropertyClass::Object, "HallwayCamera", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ACameraViews, HallwayCamera), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(NewProp_HallwayCamera_MetaData, ARRAY_COUNT(NewProp_HallwayCamera_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CornerViewCameraU_MetaData[] = {
				{ "Category", "CameraViews" },
				{ "ModuleRelativePath", "CameraViews.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CornerViewCameraU = { UE4CodeGen_Private::EPropertyClass::Object, "CornerViewCameraU", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ACameraViews, CornerViewCameraU), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(NewProp_CornerViewCameraU_MetaData, ARRAY_COUNT(NewProp_CornerViewCameraU_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TopDownCamera_MetaData[] = {
				{ "Category", "CameraViews" },
				{ "ModuleRelativePath", "CameraViews.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TopDownCamera = { UE4CodeGen_Private::EPropertyClass::Object, "TopDownCamera", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ACameraViews, TopDownCamera), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(NewProp_TopDownCamera_MetaData, ARRAY_COUNT(NewProp_TopDownCamera_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerCamera_MetaData[] = {
				{ "Category", "CameraViews" },
				{ "ModuleRelativePath", "CameraViews.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerCamera = { UE4CodeGen_Private::EPropertyClass::Object, "PlayerCamera", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ACameraViews, PlayerCamera), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(NewProp_PlayerCamera_MetaData, ARRAY_COUNT(NewProp_PlayerCamera_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CornerCamera_MetaData[] = {
				{ "Category", "CameraViews" },
				{ "ModuleRelativePath", "CameraViews.h" },
				{ "ToolTip", "Makes cameras visible in editor" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CornerCamera = { UE4CodeGen_Private::EPropertyClass::Object, "CornerCamera", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, STRUCT_OFFSET(ACameraViews, CornerCamera), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(NewProp_CornerCamera_MetaData, ARRAY_COUNT(NewProp_CornerCamera_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_HallwayCamera,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CornerViewCameraU,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TopDownCamera,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_PlayerCamera,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CornerCamera,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ACameraViews>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ACameraViews::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACameraViews, 377734297);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACameraViews(Z_Construct_UClass_ACameraViews, &ACameraViews::StaticClass, TEXT("/Script/BellerNicole"), TEXT("ACameraViews"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACameraViews);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
