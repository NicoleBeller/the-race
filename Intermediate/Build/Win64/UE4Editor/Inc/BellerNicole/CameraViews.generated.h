// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BELLERNICOLE_CameraViews_generated_h
#error "CameraViews.generated.h already included, missing '#pragma once' in CameraViews.h"
#endif
#define BELLERNICOLE_CameraViews_generated_h

#define GAM_312_Source_BellerNicole_CameraViews_h_11_RPC_WRAPPERS
#define GAM_312_Source_BellerNicole_CameraViews_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define GAM_312_Source_BellerNicole_CameraViews_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACameraViews(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_ACameraViews(); \
public: \
	DECLARE_CLASS(ACameraViews, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(ACameraViews) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_CameraViews_h_11_INCLASS \
private: \
	static void StaticRegisterNativesACameraViews(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_ACameraViews(); \
public: \
	DECLARE_CLASS(ACameraViews, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(ACameraViews) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_CameraViews_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACameraViews(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACameraViews) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraViews); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraViews); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraViews(ACameraViews&&); \
	NO_API ACameraViews(const ACameraViews&); \
public:


#define GAM_312_Source_BellerNicole_CameraViews_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACameraViews(ACameraViews&&); \
	NO_API ACameraViews(const ACameraViews&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACameraViews); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACameraViews); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACameraViews)


#define GAM_312_Source_BellerNicole_CameraViews_h_11_PRIVATE_PROPERTY_OFFSET
#define GAM_312_Source_BellerNicole_CameraViews_h_8_PROLOG
#define GAM_312_Source_BellerNicole_CameraViews_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_CameraViews_h_11_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_CameraViews_h_11_RPC_WRAPPERS \
	GAM_312_Source_BellerNicole_CameraViews_h_11_INCLASS \
	GAM_312_Source_BellerNicole_CameraViews_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM_312_Source_BellerNicole_CameraViews_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_CameraViews_h_11_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_CameraViews_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_CameraViews_h_11_INCLASS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_CameraViews_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM_312_Source_BellerNicole_CameraViews_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
