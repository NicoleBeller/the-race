// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BELLERNICOLE_PlayerController_GAM312_generated_h
#error "PlayerController_GAM312.generated.h already included, missing '#pragma once' in PlayerController_GAM312.h"
#endif
#define BELLERNICOLE_PlayerController_GAM312_generated_h

#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_RPC_WRAPPERS
#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlayerController_GAM312(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_APlayerController_GAM312(); \
public: \
	DECLARE_CLASS(APlayerController_GAM312, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(APlayerController_GAM312) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAPlayerController_GAM312(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_APlayerController_GAM312(); \
public: \
	DECLARE_CLASS(APlayerController_GAM312, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(APlayerController_GAM312) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerController_GAM312(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerController_GAM312) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerController_GAM312); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerController_GAM312); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerController_GAM312(APlayerController_GAM312&&); \
	NO_API APlayerController_GAM312(const APlayerController_GAM312&); \
public:


#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlayerController_GAM312(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlayerController_GAM312(APlayerController_GAM312&&); \
	NO_API APlayerController_GAM312(const APlayerController_GAM312&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlayerController_GAM312); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlayerController_GAM312); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlayerController_GAM312)


#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_PRIVATE_PROPERTY_OFFSET
#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_11_PROLOG
#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_RPC_WRAPPERS \
	GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_INCLASS \
	GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_INCLASS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM_312_Source_BellerNicole_ModuleSix_PlayerController_GAM312_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
