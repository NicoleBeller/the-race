// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BELLERNICOLE_Actionables_generated_h
#error "Actionables.generated.h already included, missing '#pragma once' in Actionables.h"
#endif
#define BELLERNICOLE_Actionables_generated_h

#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_RPC_WRAPPERS
#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAActionables(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AActionables(); \
public: \
	DECLARE_CLASS(AActionables, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AActionables) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAActionables(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AActionables(); \
public: \
	DECLARE_CLASS(AActionables, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AActionables) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AActionables(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AActionables) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActionables); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActionables); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActionables(AActionables&&); \
	NO_API AActionables(const AActionables&); \
public:


#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActionables(AActionables&&); \
	NO_API AActionables(const AActionables&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActionables); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActionables); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AActionables)


#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ThisSceneComp() { return STRUCT_OFFSET(AActionables, ThisSceneComp); } \
	FORCEINLINE static uint32 __PPO__ThisMeshComp() { return STRUCT_OFFSET(AActionables, ThisMeshComp); } \
	FORCEINLINE static uint32 __PPO__ThisSphereComp() { return STRUCT_OFFSET(AActionables, ThisSphereComp); }


#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_10_PROLOG
#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_RPC_WRAPPERS \
	GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_INCLASS \
	GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_INCLASS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_ModuleSix_Actionables_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM_312_Source_BellerNicole_ModuleSix_Actionables_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
