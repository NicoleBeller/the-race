// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BELLERNICOLE_MrDude_generated_h
#error "MrDude.generated.h already included, missing '#pragma once' in MrDude.h"
#endif
#define BELLERNICOLE_MrDude_generated_h

#define GAM_312_Source_BellerNicole_MrDude_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCollectibleItem) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetCollectibleItem(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddCollectibleItem) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddCollectibleItem(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateCurrentStamina) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Stamina); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateCurrentStamina(Z_Param_Stamina); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentStamina(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetInitialStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetInitialStamina(); \
		P_NATIVE_END; \
	}


#define GAM_312_Source_BellerNicole_MrDude_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCollectibleItem) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetCollectibleItem(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execAddCollectibleItem) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->AddCollectibleItem(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execUpdateCurrentStamina) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Stamina); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->UpdateCurrentStamina(Z_Param_Stamina); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCurrentStamina(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetInitialStamina) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetInitialStamina(); \
		P_NATIVE_END; \
	}


#define GAM_312_Source_BellerNicole_MrDude_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMrDude(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AMrDude(); \
public: \
	DECLARE_CLASS(AMrDude, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AMrDude) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_MrDude_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMrDude(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AMrDude(); \
public: \
	DECLARE_CLASS(AMrDude, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AMrDude) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_MrDude_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMrDude(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMrDude) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMrDude); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMrDude); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMrDude(AMrDude&&); \
	NO_API AMrDude(const AMrDude&); \
public:


#define GAM_312_Source_BellerNicole_MrDude_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMrDude(AMrDude&&); \
	NO_API AMrDude(const AMrDude&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMrDude); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMrDude); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMrDude)


#define GAM_312_Source_BellerNicole_MrDude_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AMrDude, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AMrDude, FollowCamera); } \
	FORCEINLINE static uint32 __PPO__InitialStamina() { return STRUCT_OFFSET(AMrDude, InitialStamina); } \
	FORCEINLINE static uint32 __PPO__CurrentStamina() { return STRUCT_OFFSET(AMrDude, CurrentStamina); } \
	FORCEINLINE static uint32 __PPO__CollectibleItem() { return STRUCT_OFFSET(AMrDude, CollectibleItem); }


#define GAM_312_Source_BellerNicole_MrDude_h_9_PROLOG
#define GAM_312_Source_BellerNicole_MrDude_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_MrDude_h_12_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_MrDude_h_12_RPC_WRAPPERS \
	GAM_312_Source_BellerNicole_MrDude_h_12_INCLASS \
	GAM_312_Source_BellerNicole_MrDude_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM_312_Source_BellerNicole_MrDude_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_MrDude_h_12_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_MrDude_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_MrDude_h_12_INCLASS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_MrDude_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM_312_Source_BellerNicole_MrDude_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
