// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "ModuleSix/CollectibleItems.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCollectibleItems() {}
// Cross Module References
	BELLERNICOLE_API UClass* Z_Construct_UClass_ACollectibleItems_NoRegister();
	BELLERNICOLE_API UClass* Z_Construct_UClass_ACollectibleItems();
	BELLERNICOLE_API UClass* Z_Construct_UClass_AActionables();
	UPackage* Z_Construct_UPackage__Script_BellerNicole();
// End Cross Module References
	void ACollectibleItems::StaticRegisterNativesACollectibleItems()
	{
	}
	UClass* Z_Construct_UClass_ACollectibleItems_NoRegister()
	{
		return ACollectibleItems::StaticClass();
	}
	UClass* Z_Construct_UClass_ACollectibleItems()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActionables,
				(UObject* (*)())Z_Construct_UPackage__Script_BellerNicole,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "ModuleSix/CollectibleItems.h" },
				{ "ModuleRelativePath", "ModuleSix/CollectibleItems.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ACollectibleItems>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ACollectibleItems::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACollectibleItems, 2836715791);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACollectibleItems(Z_Construct_UClass_ACollectibleItems, &ACollectibleItems::StaticClass, TEXT("/Script/BellerNicole"), TEXT("ACollectibleItems"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACollectibleItems);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
