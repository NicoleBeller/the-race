// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BELLERNICOLE_BellerNicoleGameModeBase_generated_h
#error "BellerNicoleGameModeBase.generated.h already included, missing '#pragma once' in BellerNicoleGameModeBase.h"
#endif
#define BELLERNICOLE_BellerNicoleGameModeBase_generated_h

#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_RPC_WRAPPERS
#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABellerNicoleGameModeBase(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_ABellerNicoleGameModeBase(); \
public: \
	DECLARE_CLASS(ABellerNicoleGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(ABellerNicoleGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABellerNicoleGameModeBase(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_ABellerNicoleGameModeBase(); \
public: \
	DECLARE_CLASS(ABellerNicoleGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(ABellerNicoleGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABellerNicoleGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABellerNicoleGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABellerNicoleGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABellerNicoleGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABellerNicoleGameModeBase(ABellerNicoleGameModeBase&&); \
	NO_API ABellerNicoleGameModeBase(const ABellerNicoleGameModeBase&); \
public:


#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABellerNicoleGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABellerNicoleGameModeBase(ABellerNicoleGameModeBase&&); \
	NO_API ABellerNicoleGameModeBase(const ABellerNicoleGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABellerNicoleGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABellerNicoleGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABellerNicoleGameModeBase)


#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_12_PROLOG
#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_RPC_WRAPPERS \
	GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_INCLASS \
	GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM_312_Source_BellerNicole_BellerNicoleGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
