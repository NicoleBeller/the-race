// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PawnCollision.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePawnCollision() {}
// Cross Module References
	BELLERNICOLE_API UClass* Z_Construct_UClass_APawnCollision_NoRegister();
	BELLERNICOLE_API UClass* Z_Construct_UClass_APawnCollision();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_BellerNicole();
// End Cross Module References
	void APawnCollision::StaticRegisterNativesAPawnCollision()
	{
	}
	UClass* Z_Construct_UClass_APawnCollision_NoRegister()
	{
		return APawnCollision::StaticClass();
	}
	UClass* Z_Construct_UClass_APawnCollision()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_APawn,
				(UObject* (*)())Z_Construct_UPackage__Script_BellerNicole,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Navigation" },
				{ "IncludePath", "PawnCollision.h" },
				{ "ModuleRelativePath", "PawnCollision.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APawnCollision>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APawnCollision::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APawnCollision, 2375435998);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APawnCollision(Z_Construct_UClass_APawnCollision, &APawnCollision::StaticClass, TEXT("/Script/BellerNicole"), TEXT("APawnCollision"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APawnCollision);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
