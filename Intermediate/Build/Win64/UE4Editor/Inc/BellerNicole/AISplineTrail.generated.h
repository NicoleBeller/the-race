// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BELLERNICOLE_AISplineTrail_generated_h
#error "AISplineTrail.generated.h already included, missing '#pragma once' in AISplineTrail.h"
#endif
#define BELLERNICOLE_AISplineTrail_generated_h

#define GAM_312_Source_BellerNicole_AISplineTrail_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSplinePoints) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetSplinePoints(); \
		P_NATIVE_END; \
	}


#define GAM_312_Source_BellerNicole_AISplineTrail_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSplinePoints) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->GetSplinePoints(); \
		P_NATIVE_END; \
	}


#define GAM_312_Source_BellerNicole_AISplineTrail_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAISplineTrail(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AAISplineTrail(); \
public: \
	DECLARE_CLASS(AAISplineTrail, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AAISplineTrail) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_AISplineTrail_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAAISplineTrail(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AAISplineTrail(); \
public: \
	DECLARE_CLASS(AAISplineTrail, AAIController, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AAISplineTrail) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define GAM_312_Source_BellerNicole_AISplineTrail_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAISplineTrail(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAISplineTrail) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAISplineTrail); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAISplineTrail); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAISplineTrail(AAISplineTrail&&); \
	NO_API AAISplineTrail(const AAISplineTrail&); \
public:


#define GAM_312_Source_BellerNicole_AISplineTrail_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAISplineTrail(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAISplineTrail(AAISplineTrail&&); \
	NO_API AAISplineTrail(const AAISplineTrail&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAISplineTrail); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAISplineTrail); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAISplineTrail)


#define GAM_312_Source_BellerNicole_AISplineTrail_h_17_PRIVATE_PROPERTY_OFFSET
#define GAM_312_Source_BellerNicole_AISplineTrail_h_14_PROLOG
#define GAM_312_Source_BellerNicole_AISplineTrail_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_AISplineTrail_h_17_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_AISplineTrail_h_17_RPC_WRAPPERS \
	GAM_312_Source_BellerNicole_AISplineTrail_h_17_INCLASS \
	GAM_312_Source_BellerNicole_AISplineTrail_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define GAM_312_Source_BellerNicole_AISplineTrail_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	GAM_312_Source_BellerNicole_AISplineTrail_h_17_PRIVATE_PROPERTY_OFFSET \
	GAM_312_Source_BellerNicole_AISplineTrail_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_AISplineTrail_h_17_INCLASS_NO_PURE_DECLS \
	GAM_312_Source_BellerNicole_AISplineTrail_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID GAM_312_Source_BellerNicole_AISplineTrail_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
