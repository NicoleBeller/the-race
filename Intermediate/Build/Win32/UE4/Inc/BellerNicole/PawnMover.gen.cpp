// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PawnMover.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePawnMover() {}
// Cross Module References
	BELLERNICOLE_API UClass* Z_Construct_UClass_UPawnMover_NoRegister();
	BELLERNICOLE_API UClass* Z_Construct_UClass_UPawnMover();
	ENGINE_API UClass* Z_Construct_UClass_UPawnMovementComponent();
	UPackage* Z_Construct_UPackage__Script_BellerNicole();
// End Cross Module References
	void UPawnMover::StaticRegisterNativesUPawnMover()
	{
	}
	UClass* Z_Construct_UClass_UPawnMover_NoRegister()
	{
		return UPawnMover::StaticClass();
	}
	UClass* Z_Construct_UClass_UPawnMover()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_UPawnMovementComponent,
				(UObject* (*)())Z_Construct_UPackage__Script_BellerNicole,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "PawnMover.h" },
				{ "ModuleRelativePath", "PawnMover.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<UPawnMover>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&UPawnMover::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00B00084u,
				nullptr, 0,
				nullptr, 0,
				"Engine",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPawnMover, 1634225817);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPawnMover(Z_Construct_UClass_UPawnMover, &UPawnMover::StaticClass, TEXT("/Script/BellerNicole"), TEXT("UPawnMover"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPawnMover);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
