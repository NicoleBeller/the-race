// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BELLERNICOLE_PawnMover_generated_h
#error "PawnMover.generated.h already included, missing '#pragma once' in PawnMover.h"
#endif
#define BELLERNICOLE_PawnMover_generated_h

#define BellerNicole_Source_BellerNicole_PawnMover_h_13_RPC_WRAPPERS
#define BellerNicole_Source_BellerNicole_PawnMover_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define BellerNicole_Source_BellerNicole_PawnMover_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPawnMover(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_UPawnMover(); \
public: \
	DECLARE_CLASS(UPawnMover, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(UPawnMover) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BellerNicole_Source_BellerNicole_PawnMover_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUPawnMover(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_UPawnMover(); \
public: \
	DECLARE_CLASS(UPawnMover, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(UPawnMover) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BellerNicole_Source_BellerNicole_PawnMover_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPawnMover(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPawnMover) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPawnMover); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPawnMover); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPawnMover(UPawnMover&&); \
	NO_API UPawnMover(const UPawnMover&); \
public:


#define BellerNicole_Source_BellerNicole_PawnMover_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPawnMover(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPawnMover(UPawnMover&&); \
	NO_API UPawnMover(const UPawnMover&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPawnMover); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPawnMover); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPawnMover)


#define BellerNicole_Source_BellerNicole_PawnMover_h_13_PRIVATE_PROPERTY_OFFSET
#define BellerNicole_Source_BellerNicole_PawnMover_h_10_PROLOG
#define BellerNicole_Source_BellerNicole_PawnMover_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BellerNicole_Source_BellerNicole_PawnMover_h_13_PRIVATE_PROPERTY_OFFSET \
	BellerNicole_Source_BellerNicole_PawnMover_h_13_RPC_WRAPPERS \
	BellerNicole_Source_BellerNicole_PawnMover_h_13_INCLASS \
	BellerNicole_Source_BellerNicole_PawnMover_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BellerNicole_Source_BellerNicole_PawnMover_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BellerNicole_Source_BellerNicole_PawnMover_h_13_PRIVATE_PROPERTY_OFFSET \
	BellerNicole_Source_BellerNicole_PawnMover_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	BellerNicole_Source_BellerNicole_PawnMover_h_13_INCLASS_NO_PURE_DECLS \
	BellerNicole_Source_BellerNicole_PawnMover_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BellerNicole_Source_BellerNicole_PawnMover_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
