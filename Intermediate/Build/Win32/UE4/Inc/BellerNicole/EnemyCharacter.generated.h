// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BELLERNICOLE_EnemyCharacter_generated_h
#error "EnemyCharacter.generated.h already included, missing '#pragma once' in EnemyCharacter.h"
#endif
#define BELLERNICOLE_EnemyCharacter_generated_h

#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_RPC_WRAPPERS
#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyCharacter(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AEnemyCharacter(); \
public: \
	DECLARE_CLASS(AEnemyCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AEnemyCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyCharacter(); \
	friend BELLERNICOLE_API class UClass* Z_Construct_UClass_AEnemyCharacter(); \
public: \
	DECLARE_CLASS(AEnemyCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/BellerNicole"), NO_API) \
	DECLARE_SERIALIZER(AEnemyCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyCharacter(AEnemyCharacter&&); \
	NO_API AEnemyCharacter(const AEnemyCharacter&); \
public:


#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyCharacter(AEnemyCharacter&&); \
	NO_API AEnemyCharacter(const AEnemyCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemyCharacter)


#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_PRIVATE_PROPERTY_OFFSET
#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_7_PROLOG
#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_PRIVATE_PROPERTY_OFFSET \
	BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_RPC_WRAPPERS \
	BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_INCLASS \
	BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_PRIVATE_PROPERTY_OFFSET \
	BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_INCLASS_NO_PURE_DECLS \
	BellerNicole_Source_BellerNicole_EnemyCharacter_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BellerNicole_Source_BellerNicole_EnemyCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
