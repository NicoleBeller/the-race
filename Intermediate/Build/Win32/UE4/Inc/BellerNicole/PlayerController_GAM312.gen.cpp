// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "ModuleSix/PlayerController_GAM312.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerController_GAM312() {}
// Cross Module References
	BELLERNICOLE_API UClass* Z_Construct_UClass_APlayerController_GAM312_NoRegister();
	BELLERNICOLE_API UClass* Z_Construct_UClass_APlayerController_GAM312();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_BellerNicole();
// End Cross Module References
	void APlayerController_GAM312::StaticRegisterNativesAPlayerController_GAM312()
	{
	}
	UClass* Z_Construct_UClass_APlayerController_GAM312_NoRegister()
	{
		return APlayerController_GAM312::StaticClass();
	}
	UClass* Z_Construct_UClass_APlayerController_GAM312()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_APlayerController,
				(UObject* (*)())Z_Construct_UPackage__Script_BellerNicole,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "ModuleSix/PlayerController_GAM312.h" },
				{ "ModuleRelativePath", "ModuleSix/PlayerController_GAM312.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APlayerController_GAM312>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APlayerController_GAM312::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900284u,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlayerController_GAM312, 415831658);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlayerController_GAM312(Z_Construct_UClass_APlayerController_GAM312, &APlayerController_GAM312::StaticClass, TEXT("/Script/BellerNicole"), TEXT("APlayerController_GAM312"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayerController_GAM312);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
