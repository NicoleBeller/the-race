// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "BellerNicoleGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBellerNicoleGameModeBase() {}
// Cross Module References
	BELLERNICOLE_API UClass* Z_Construct_UClass_ABellerNicoleGameModeBase_NoRegister();
	BELLERNICOLE_API UClass* Z_Construct_UClass_ABellerNicoleGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_BellerNicole();
// End Cross Module References
	void ABellerNicoleGameModeBase::StaticRegisterNativesABellerNicoleGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ABellerNicoleGameModeBase_NoRegister()
	{
		return ABellerNicoleGameModeBase::StaticClass();
	}
	UClass* Z_Construct_UClass_ABellerNicoleGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_BellerNicole,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "BellerNicoleGameModeBase.h" },
				{ "ModuleRelativePath", "BellerNicoleGameModeBase.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ABellerNicoleGameModeBase>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ABellerNicoleGameModeBase::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABellerNicoleGameModeBase, 3170839754);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABellerNicoleGameModeBase(Z_Construct_UClass_ABellerNicoleGameModeBase, &ABellerNicoleGameModeBase::StaticClass, TEXT("/Script/BellerNicole"), TEXT("ABellerNicoleGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABellerNicoleGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
