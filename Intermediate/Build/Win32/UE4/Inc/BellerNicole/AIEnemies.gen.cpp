// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "AIEnemies.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAIEnemies() {}
// Cross Module References
	BELLERNICOLE_API UClass* Z_Construct_UClass_AAIEnemies_NoRegister();
	BELLERNICOLE_API UClass* Z_Construct_UClass_AAIEnemies();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_BellerNicole();
	BELLERNICOLE_API UFunction* Z_Construct_UFunction_AAIEnemies_GetRandomWaypoint();
	ENGINE_API UClass* Z_Construct_UClass_ATargetPoint_NoRegister();
	BELLERNICOLE_API UFunction* Z_Construct_UFunction_AAIEnemies_GoToRandomWaypoint();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void AAIEnemies::StaticRegisterNativesAAIEnemies()
	{
		UClass* Class = AAIEnemies::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetRandomWaypoint", &AAIEnemies::execGetRandomWaypoint },
			{ "GoToRandomWaypoint", &AAIEnemies::execGoToRandomWaypoint },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AAIEnemies_GetRandomWaypoint()
	{
		struct AIEnemies_eventGetRandomWaypoint_Parms
		{
			ATargetPoint* ReturnValue;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000580, 1, nullptr, STRUCT_OFFSET(AIEnemies_eventGetRandomWaypoint_Parms, ReturnValue), Z_Construct_UClass_ATargetPoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ReturnValue,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "AIEnemies.h" },
				{ "ToolTip", "Random target points" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AAIEnemies, "GetRandomWaypoint", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, sizeof(AIEnemies_eventGetRandomWaypoint_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AAIEnemies_GoToRandomWaypoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "AIEnemies.h" },
				{ "ToolTip", "Random waypoints" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AAIEnemies, "GoToRandomWaypoint", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AAIEnemies_NoRegister()
	{
		return AAIEnemies::StaticClass();
	}
	UClass* Z_Construct_UClass_AAIEnemies()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AAIController,
				(UObject* (*)())Z_Construct_UPackage__Script_BellerNicole,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AAIEnemies_GetRandomWaypoint, "GetRandomWaypoint" }, // 2559893216
				{ &Z_Construct_UFunction_AAIEnemies_GoToRandomWaypoint, "GoToRandomWaypoint" }, // 2392073885
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "AIEnemies.h" },
				{ "ModuleRelativePath", "AIEnemies.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsPathRandom_MetaData[] = {
				{ "Category", "AIEnemies" },
				{ "ModuleRelativePath", "AIEnemies.h" },
				{ "ToolTip", "To be added to the \"Random AI\" blueprint" },
			};
#endif
			auto NewProp_bIsPathRandom_SetBit = [](void* Obj){ ((AAIEnemies*)Obj)->bIsPathRandom = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsPathRandom = { UE4CodeGen_Private::EPropertyClass::Bool, "bIsPathRandom", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(AAIEnemies), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bIsPathRandom_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bIsPathRandom_MetaData, ARRAY_COUNT(NewProp_bIsPathRandom_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Waypoints_MetaData[] = {
				{ "ModuleRelativePath", "AIEnemies.h" },
				{ "ToolTip", "Waypoints for AI to stop/hit" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Waypoints = { UE4CodeGen_Private::EPropertyClass::Array, "Waypoints", RF_Public|RF_Transient|RF_MarkAsNative, 0x0040000000000000, 1, nullptr, STRUCT_OFFSET(AAIEnemies, Waypoints), METADATA_PARAMS(NewProp_Waypoints_MetaData, ARRAY_COUNT(NewProp_Waypoints_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Waypoints_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "Waypoints", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bIsPathRandom,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Waypoints,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Waypoints_Inner,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AAIEnemies>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AAIEnemies::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900280u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAIEnemies, 3218955007);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAIEnemies(Z_Construct_UClass_AAIEnemies, &AAIEnemies::StaticClass, TEXT("/Script/BellerNicole"), TEXT("AAIEnemies"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAIEnemies);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
