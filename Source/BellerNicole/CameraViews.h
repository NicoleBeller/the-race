#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "CameraViews.generated.h"

UCLASS()
class BELLERNICOLE_API ACameraViews : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraViews();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Creates a camera controller
	APlayerController* OurPlayerController;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Makes cameras visible in editor
	UPROPERTY(EditAnywhere)
		AActor *CornerCamera;
	UPROPERTY(EditAnywhere)
		AActor *PlayerCamera;
	UPROPERTY(EditAnywhere)
		AActor *TopDownCamera;
	UPROPERTY(EditAnywhere)
		AActor *CornerViewCameraU;
	UPROPERTY(EditAnywhere)
		AActor *HallwayCamera;

	// Sets view to specific cameras
	void SetTopDownCam();
	void SetPlayerCam();
	void SetCornerCam();
	void SetCornerViewCamU();
	void SetHallwayCam();
};
