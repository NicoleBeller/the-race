/*
**		Section Four: Follow A Path. This is my spline path.  My AI character, located in the back corner of the upper floor in my level, follows 
**		this specific path.  For this path, there are multiple points on a spline (line).  This AI character follows the line and hits every 
**		point, before looping over and over again.  The AI also avoids collisions and will keep trying to run, even when they hit something.
*/

#include "AISplineTrail.h"


// Height variable
const int HEIGHT = 250;

// Max point variable 
const int MAXPOINTS = 5000;

// Vector locations
FVector trailPointLocation[MAXPOINTS];

// Pointer for spline variable
int splinePointer = 1;

// Total spline point variable
int totalSplinePoints = 0;

// When game starts, AI will begin moving
void AAISplineTrail::BeginPlay()
{
	Super::BeginPlay();

	GetSplinePoints();
	MoveToLocation(trailPointLocation[splinePointer]);
}

// AI movement
void AAISplineTrail::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	splinePointer++;
	if (splinePointer >= totalSplinePoints)
	{
		splinePointer = 1;
	}
	MoveToLocation(trailPointLocation[splinePointer]);
}

// Gathers information about the spline location and points; Spline information
void AAISplineTrail::GetSplinePoints()
{

	for (TObjectIterator<USplineComponent> SplineComponent; SplineComponent; ++SplineComponent)
	{
		// Number of spline points
		int numberOfSplinePoints = SplineComponent->GetNumberOfSplinePoints();
		// Length of the spline path
		float totalLength = SplineComponent->GetSplineLength();

		// Goes through the spline
		float currentLength = 0;
		// items that are spawned along the spline need spaces
		int itemSpacing = 5;
		// samples the spline every specific few units
		int sampleLength = 150;

		FString splineName = SplineComponent->GetName();

		if (splineName == "TrailSpline")
		{
			// Starts at the beginning of the spline
			int splinePointCount = 0;
			while (currentLength < totalLength)
			{
				FTransform splinePointTransform = SplineComponent->GetTransformAtDistanceAlongSpline(currentLength, ESplineCoordinateSpace::World);
				// Increases current spline length
				currentLength += sampleLength;
				trailPointLocation[splinePointCount] = splinePointTransform.GetLocation();
				splinePointCount++;
			}
			totalSplinePoints = splinePointCount;
		}
	}

}