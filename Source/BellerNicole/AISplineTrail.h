#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "Navigation/PathFollowingComponent.h"
#include "AISplineTrail.generated.h"


UCLASS()
class BELLERNICOLE_API AAISplineTrail : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

private:
	// Makes a SplineLinePath component
	USplineComponent* MySplineLinePath;

	// Gets points for the spline path
	UFUNCTION()
		void GetSplinePoints();

	// Gets number of spline points in the spline path
	int32 myNumberOfSplinePoints;

};
