/*
**		Section Four: Follow a Path.  This is my target point path.  My AI will either run randomly, to randomized waypoints (target points) or they
**		will run to the next waypoint in the order of 1 to 2 to 3, and so on.  These AI will run to each waypoint and wait a second before
**		they run to their next waypoint.  This makes it easy to keep track of them, to watch or follow them, and I honestly just really like being
**		able to have so much control over the AI.
*/
#include "AIEnemies.h"

// When game starts, AI will begin moving
void AAIEnemies::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATargetPoint::StaticClass(), Waypoints);
	WPIndexCount = Waypoints.Num();
	GoToNextWaypoint();
}

// AI movement
void AAIEnemies::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult & Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	if (bIsPathRandom)
	{
		// Sets world timer, AI goes to random waypoint
		GetWorldTimerManager().SetTimer(TimerHandle, this, &AAIEnemies::GoToRandomWaypoint, 1.0f, false);
	}
	else
	{
		// Sets world timer, AI continues on a set path
		GetWorldTimerManager().SetTimer(TimerHandle, this, &AAIEnemies::GoToNextWaypoint, 1.0f, false);
	}
}

// Gets the random waypoint
ATargetPoint * AAIEnemies::GetRandomWaypoint()
{
	// random number generator equal to number of waypoints
	auto index = FMath::RandRange(0, Waypoints.Num() - 1);
	return Cast<ATargetPoint>(Waypoints[index]);
}

// AI moves to a random waypoint
void AAIEnemies::GoToRandomWaypoint()
{
	MoveToActor(GetRandomWaypoint());
}

// AI moves to next waypoint
void AAIEnemies::GoToNextWaypoint()
{
	MoveToActor(Waypoints[currentWPIndex]);
	currentWPIndex++;

	// Verifies minimum and maximum number of points
	int Cycle = currentWPIndex % WPIndexCount;

	// Keeps AI moving
	if (currentWPIndex >= WPIndexCount - 1)
	{
		currentWPIndex = 0;
	}
}
