/*
**		Section Three: Collision Detection.  This code allows for interaction between the player and inanimate objects.  Specifically, items will be
**		scattered throughout the level, and the player can 'pick them up'.  When the player goes into the collision box, there will be text displayed,
**		showing that there is something unique about this item.  (The collision box is placed directly over the entire item, to make sure that the player
**		will not miss the opportunity to see the text, and realize that the item is special.)  If the player chooses to walk over the item, they ultimately 
**		'pick it up' and then additional text will appear on the screen, signifying that the item has indeed been picked up.  Last term I learned about
**		'Simulate Physics' in the Unreal Engine.  When that box is checked, objects that are placed high into the air come crashing down.  That makes some
**		pretty awesome explosions, to be honest.  For this, however, game physics is used a little bit differently.  Nothing is exploding in my game, but
**		the (re)moveable objects are affected by physics because as the player walks over the item, it disappears.  The player can collide with the item, but
**		they can't collide with and remove the AI or the walls or stairs.
*/



#include "Pickup.h"
#include "Engine.h"
#include "DrawDebugHelpers.h"

// Sets default values
APickup::APickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Pickup root component
	PickupRoot = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = PickupRoot;

	// Creates a mesh for the pickup
	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MESH"));
	PickupMesh->AttachToComponent(PickupRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);

	// Creates a collision box
	PickupBox = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupBox"));
	PickupBox->SetWorldScale3D(FVector(1.0f, 1.0f, 1.0f));
	PickupBox->bGenerateOverlapEvents = true;
	PickupBox->AttachToComponent(PickupRoot, FAttachmentTransformRules::SnapToTargetIncludingScale);
	PickupBox->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnOverlapBegin);
	PickupBox->OnComponentEndOverlap.AddDynamic(this, &APickup::OnOverlapEnd);
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Adds text to screen when player approaches collision box
void APickup::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	DrawDebugString(GetWorld(), GetActorLocation(), "*GASP*", nullptr, FColor::White, 3.0f, true);
}

// Displays text to screen when player walks away from collision box
void APickup::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	DrawDebugString(GetWorld(), GetActorLocation(), "Fooled 'ya!", nullptr, FColor::Red, 3.0f, true);
	Destroy();
}