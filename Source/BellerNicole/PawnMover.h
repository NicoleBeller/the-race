#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PawnMovementComponent.h"
#include "PawnMover.generated.h"

/**
 * 
 */
UCLASS()
class BELLERNICOLE_API UPawnMover : public UPawnMovementComponent
{
	GENERATED_BODY()

		// Runs every tick of the game
		virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	
	
};
