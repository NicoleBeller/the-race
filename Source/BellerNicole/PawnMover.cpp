/*
**		This goes along with the PawnCollision files.  This lets the sphere pawn move.
**
*/


#include "PawnMover.h"

// Runs every tick of the game
void UPawnMover::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PawnOwner || !UpdatedComponent || ShouldSkipUpdate(DeltaTime))
	{
		// Gets out of function if it's not the PawnMover, updated component is false, or if skip update is true 
		return;
	}

	// Holds movement per frame, calculates movement
	FVector DesiredMovementThisFrame = ConsumeInputVector().GetClampedToMaxSize(1.0f) * DeltaTime * 150.0f;

	// Checks to see if PawnMover is hitting an object
	if (!DesiredMovementThisFrame.IsNearlyZero())
	{
		FHitResult Hit;
		SafeMoveUpdatedComponent(DesiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, Hit);

		// PawnMover slides along surface, if it's hitting something
		if (Hit.IsValidBlockingHit())
		{
			SlideAlongSurface(DesiredMovementThisFrame, 1.f - Hit.Time, Hit.Normal, Hit);
		}
	}

}


