/* 
**		This is my spline PATH for my AI that is located in the front path on the upper floor of my level.
**		This is pretty much a duplicate of my AISPLINETRAIL.CPP.  I wanted to have two different splines going at the same time
**		but I couldn't figure out how to add the code to the 'trail', so I just added a new spline. I would like to go back and
**		figure out how to correctly code it, though, so I have copied this file, prior to adding this new spline class.
*/


#include "AISplinePath.h"
#include "AISplineTrail.h"


 // Height variable
const int HEIGHT = 250;

// Max point variable
const int MAXPOINTS = 5000;

// Vector locations
FVector pathPointLocation[MAXPOINTS];

// Pointer for spline variable
int splinePathPointer = 1;

// Total spline point variable
int totalSplinePathPoints;

// When game starts, AI will begin moving
void AAISplinePath::BeginPlay()
{
	Super::BeginPlay();

	GetSplinePoints();
	MoveToLocation(pathPointLocation[splinePathPointer]);
}

// AI movement
void AAISplinePath::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	splinePathPointer++;
	if (splinePathPointer >= totalSplinePathPoints)
	{
		splinePathPointer = 1;
	}
	MoveToLocation(pathPointLocation[splinePathPointer]);
}

// Gathers information about the spline location and points; spline information
void AAISplinePath::GetSplinePoints()
{
	for (TObjectIterator<USplineComponent>SplineComponent; SplineComponent; ++SplineComponent)
	{
		// Number of spline points
		int numberOfSplinePoints = SplineComponent->GetNumberOfSplinePoints();
		// Length of spline path
		float totalLength = SplineComponent->GetSplineLength();

		// Goes through the spline
		float currentLength = 0;
		// Items that are spawnedalong the spline need spaces
		int itemSpacing = 5;
		// Samples the spline every specific few units
		int sampleLength = 150;

		FString splineName = SplineComponent->GetName();

		if (splineName == "PathSpline")
		{
			// Starts at the beginning of the spline
			int splinePointCount = 0;
			while (currentLength < totalLength)
			{
				FTransform splinePointTransform = SplineComponent->GetTransformAtDistanceAlongSpline(currentLength, ESplineCoordinateSpace::World);
				// Increases current spline length
				currentLength += sampleLength;
				pathPointLocation[splinePointCount] = splinePointTransform.GetLocation();
				splinePointCount++;
			}
			totalSplinePathPoints = splinePointCount;
		}
	}
}