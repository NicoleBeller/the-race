#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "MrDudeGameMode.generated.h"

/**
 * 
 */
UCLASS()
class BELLERNICOLE_API AMrDudeGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	// Controls interface
	UFUNCTION(BlueprintCallable, Category = "UMG Game")
		void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);
	
protected:
	virtual void BeginPlay() override;

	// First widget to be shown
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
		TSubclassOf<UUserWidget> StartingWidget;

	// Can cycle through interfaces
	UPROPERTY()
	class UUserWidget * CurrentWidget;

	// Creates HUD 
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Stamina", Meta = (BlueprintProtected = "true"))
		TSubclassOf<UUserWidget> PlayerHUDClass;
};
