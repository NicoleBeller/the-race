/*
**		In my game, the player plays as MrDude.  With this PawnCollision, the player can play as a Sphere instead.  Why would they want to? No idea!
**		It is an option, though.  For this code, I created the sphere pawn, added the sphere mesh to the pawn and made it so that the pawn could
**		move, just like MrDude.  I did, however, comment OUT the ability to control the sphere, because I didn't figure out how to toggle between MrDude
**		and the sphere.  As of right now, the player does NOT have access to the sphere, so I will not be actively including it into my game as of 11/30/18.
*/


#include "PawnCollision.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "ConstructorHelpers.h"
#include "PawnMover.h"

// Sets default values
APawnCollision::APawnCollision()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creates the sphere pawn
	USphereComponent* SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	RootComponent = SphereComponent;
	SphereComponent->InitSphereRadius(40.f); // PLAY WITH THIS NUMBER
	SphereComponent->SetCollisionProfileName(TEXT("Pawn"));

	// Creates the sphere mesh and places it in center of sphere
	UStaticMeshComponent* SphereVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visual"));
	SphereVisual->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	if (SphereVisualAsset.Succeeded())
	{
		SphereVisual->SetStaticMesh(SphereVisualAsset.Object);
		SphereVisual->SetRelativeLocation(FVector(0.0f, 0.0f, -40.0f));
		SphereVisual->SetWorldScale3D(FVector(0.8f));
	}

	// Becomes the sphere pawn (Commented out, so the player plays as MrDude, not the sphere pawn.)
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	// make PawnMover a root component
	OurMovementComponent = CreateDefaultSubobject<UPawnMover>(TEXT("CustomMovements"));
	OurMovementComponent->UpdatedComponent = RootComponent;
}

// Called when the game starts or when spawned
void APawnCollision::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APawnCollision::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APawnCollision::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Pawn Axis
	InputComponent->BindAxis("Forward", this, &APawnCollision::Forward);
	InputComponent->BindAxis("Right", this, &APawnCollision::Right);
	InputComponent->BindAxis("Turn", this, &APawnCollision::Turn);

}

// 
UPawnMovementComponent* APawnCollision::GetMovementComponent() const
{
	return OurMovementComponent;
}

// PawnMover can move forward and backward
void APawnCollision::Forward(float Value)
{
	if (OurMovementComponent && (OurMovementComponent->UpdatedComponent == RootComponent))
	{
		OurMovementComponent->AddInputVector(GetActorForwardVector() * Value);
	}
}

// PawnMover can move left and right
void APawnCollision::Right(float Value)
{
	if (OurMovementComponent && (OurMovementComponent->UpdatedComponent == RootComponent))
	{
		OurMovementComponent->AddInputVector(GetActorRightVector() * Value);
	}
}

// PawnMover can change rotation
void APawnCollision::Turn(float Value)
{
	FRotator NewRotation = GetActorRotation();
	NewRotation.Yaw += Value;
	SetActorRotation(NewRotation);
}

