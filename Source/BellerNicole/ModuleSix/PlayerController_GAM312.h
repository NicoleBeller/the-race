
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerController_GAM312.generated.h"

/**
 * 
 */
UCLASS()
class BELLERNICOLE_API APlayerController_GAM312 : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void BeginPlay() override;
};
