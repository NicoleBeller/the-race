
#include "Actionables.h"
#include "Engine.h"
#include "MrDude.h"
#include "Kismet/GameplayStatics.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AActionables::AActionables()
{
	ThisSceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = ThisSceneComp;

	ThisMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	ThisMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ThisMeshComp->SetupAttachment(ThisSceneComp);

	ThisSphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("Trigger"));
	ThisSphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ThisSphereComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	ThisSphereComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	ThisSphereComp->SetupAttachment(ThisSceneComp);
}

// Called when the game starts or when spawned
void AActionables::BeginPlay()
{
	Super::BeginPlay();
	
}

// Adds special effects to object
void AActionables::PlayEffect()
{
	if (PickupFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, PickupFX, GetActorLocation(), FRotator::ZeroRotator, true);
	}
}

// Plays effect when player collects (walks into) object
void AActionables::NotifyActorBeginOverlap(AActor * OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	PlayEffect();
}

// Stops effect when object is picked up
void AActionables::NotifyActorEndOverlap(AActor * OtherActor)
{
	Super::NotifyActorEndOverlap(OtherActor);
}

