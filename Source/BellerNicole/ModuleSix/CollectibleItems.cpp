
#include "CollectibleItems.h"
#include "MrDude.h"

// When player overlaps object, object should be collected and then destroyed
void ACollectibleItems::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	AMrDude* OtherCharacter = Cast<AMrDude>(OtherActor);
	if (OtherCharacter)
	{
		OtherCharacter->AddCollectibleItem(1);
		Destroy();
	}
}