
#pragma once

#include "CoreMinimal.h"
#include "ModuleSix/Actionables.h"
#include "CollectibleItems.generated.h"

/**
 * 
 */
UCLASS()
class BELLERNICOLE_API ACollectibleItems : public AActionables
{
	GENERATED_BODY()
	
protected:

public:

	// Overlap events
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
};
