#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Actionables.generated.h"


class USphereComponent;

UCLASS()
class BELLERNICOLE_API AActionables : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AActionables();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnywhere, Category = "Components")
		USceneComponent* ThisSceneComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UStaticMeshComponent* ThisMeshComp;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		USphereComponent* ThisSphereComp;

	void PlayEffect();

public:	

	UPROPERTY(EditAnywhere, Category = "Effects")
		UParticleSystem* PickupFX;

	// Overlap events
	virtual void NotifyActorBeginOverlap(AActor* OtherActor);
	virtual void NotifyActorEndOverlap(AActor* OtherActor);
	
};
