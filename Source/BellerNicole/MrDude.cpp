/*
**		Section One: Display A Character.  To display the player character in Unreal, I created a new class.  I called this new class, MrDude.  After
**		creating the class, I right-clicked on the class in the Unreal Editor and made it a blueprint.  In the blueprint, I gave it a mesh and a stance.
**		With this code, specifically, MrDude can now walk forward, backward and sideways.  MrDude can also now sprint.  Within this code, the player can
**		also toggle between all of the camera views.  
*/

#include "MrDude.h"
#include "Components/InputComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/Character.h"
#include "CameraViews.h"

// Sets default values
AMrDude::AMrDude()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// Player's view
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Moves camera with mouse
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);

	// Creates camera and attaches it to MrDude
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300; 
	CameraBoom->bUsePawnControlRotation = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	// Stamina information
	InitialStamina = 100.f;
	CurrentStamina = InitialStamina;

}

// Called when the game starts or when spawned
void AMrDude::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AMrDude::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime); 

	// Drains stamina slowly 
	UpdateCurrentStamina(-DeltaTime * 0.01f * (InitialStamina));  // CAN DRAIN STAMINA FASTER

}

float AMrDude::GetInitialStamina()
{
	return InitialStamina;
}

float AMrDude::GetCurrentStamina()
{
	return CurrentStamina;
}

void AMrDude::UpdateCurrentStamina(float Stamina)
{
	CurrentStamina = CurrentStamina + Stamina;
}

// Called to bind functionality to input
void AMrDude::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Character Actions
	InputComponent->BindAction("Sprinting", IE_Pressed, this, &AMrDude::EnableSprint);
	InputComponent->BindAction("Sprinting", IE_Released, this, &AMrDude::DisableSprint);

	PlayerInputComponent->BindAction("PlayerCam", IE_Pressed, this, &AMrDude::ChangeView<0>);
	PlayerInputComponent->BindAction("CornerCam", IE_Pressed, this, &AMrDude::ChangeView<1>);
	PlayerInputComponent->BindAction("TopDownCam", IE_Pressed, this, &AMrDude::ChangeView<2>);
	PlayerInputComponent->BindAction("CornerViewCamU", IE_Pressed, this, &AMrDude::ChangeView<3>);
	PlayerInputComponent->BindAction("HallwayCam", IE_Pressed, this, &AMrDude::ChangeView<4>);
	PlayerInputComponent->BindAction("ToggleCam", IE_Pressed, this, &AMrDude::ChangeView<5>);

	// Character Axis
	InputComponent->BindAxis("Forward", this, &AMrDude::Forward);
	InputComponent->BindAxis("Right", this, &AMrDude::Right);

	InputComponent->BindAxis("Turn", this, &AMrDude::AddControllerYawInput);
	InputComponent->BindAxis("LookUp", this, &AMrDude::AddControllerPitchInput);
	InputComponent->BindAxis("TurnRate", this, &AMrDude::TurnAtRate);
	InputComponent->BindAxis("LookUpRate", this, &AMrDude::LookUpAtRate);
}

void AMrDude::AddCollectibleItem(int Value)
{
	CollectibleItem += Value;
}

int AMrDude::GetCollectibleItem()
{
	return CollectibleItem;
}

// Character can move forward and backward
void AMrDude::Forward(float Value)
{
	if (Controller && Value)
	{
		// Increases walk speed (character can sprint)
		if (bIsSprinting)
			Value *= 2;
	
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);			  //Original Code: GetActorForwardVector();
		AddMovementInput(Direction, Value / 2);
	}
}

// Character can move left and right
void AMrDude::Right(float Value)
{
	if (Controller && Value)
	{
		// Increases walk speed (character can sprint)
		if (bIsSprinting)
			Value *= 2;
	
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);          //Original Code: GetActorRightVector();
		AddMovementInput(Direction, Value / 2);
	}
}

// Character can sprint
void AMrDude::EnableSprint()
{
	// Toggles sprinting feature
	bIsSprinting = true;
}

// Character stops sprinting
void AMrDude::DisableSprint()
{
	bIsSprinting = false;
}

// Controls the way MrDude moves
void AMrDude::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

// Controls the way MrDude moves
void AMrDude::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookUpRate* GetWorld()->GetDeltaSeconds());
}

// Changes camera views
void AMrDude::ChangeView(int32 Cam)
{
	for (TActorIterator<ACameraViews> Itr(GetWorld()); Itr; ++Itr)
	{
		if (Cam == 0)
		{
			Itr->SetPlayerCam();
			break;
		}
		else if (Cam == 1)
		{
			Itr->SetCornerCam();
			break;
		}
		else if (Cam == 2)
		{
			Itr->SetTopDownCam();
			break;
		}
		else if (Cam == 3)
		{
			Itr->SetCornerViewCamU();
			break;
		}
		else if (Cam == 4)
		{
			Itr->SetHallwayCam();
			break;
		}
		else if (Cam == 5)
		{
			Itr->SetPlayerCam();
			AMrDude::TogglePlayerCam();
			break;
		}
	}
}

// Players can toggle between first and third person
void AMrDude::TogglePlayerCam()
{
	if (bIsThirdPerson)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Set to First Person"));
		CameraBoom->TargetArmLength = 0;
		bIsThirdPerson = false;

		// Hides the player when in first person
		ACharacter::SetActorHiddenInGame(true);

		bUseControllerRotationPitch = true;
		bUseControllerRotationRoll = true;
		bUseControllerRotationYaw = true;
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, TEXT("Set to Third Person"));
		// Camera should be further away from player, to show it's a third-person view
		CameraBoom->TargetArmLength = 300;
		bIsThirdPerson = true;

		ACharacter::SetActorHiddenInGame(false);

		bUseControllerRotationPitch = false;
		bUseControllerRotationRoll = false;
		bUseControllerRotationYaw = false;
	}
}