#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "MrDude.generated.h"

UCLASS()
class BELLERNICOLE_API AMrDude : public ACharacter
{
	GENERATED_BODY()

		UPROPERTY(VisibleAnywhere, BluePrintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BluePrintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

public:
	// Sets default values for this character's properties
	AMrDude();

	// Player view controls
	UPROPERTY(VisibleAnywhere, BluePrintReadOnly, Category = Camera)
		float BaseTurnRate;
	UPROPERTY(VisibleAnywhere, BluePrintReadOnly, Category = Camera)
		float BaseLookUpRate;

	// Accessor function for initial stamina
	UFUNCTION(BlueprintPure, Category = "Stamina")
		float GetInitialStamina();

	// Accessor for current stamina
	UFUNCTION(BlueprintPure, Category = "Stamina")
		float GetCurrentStamina();

	// Updates player's current stamina
	// @param Stamina This is the amount to change the player's stamina. Can be positive or negative.
	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void UpdateCurrentStamina(float Stamina);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Toggles between first person and third person
	bool bIsThirdPerson = true;
	void TogglePlayerCam();

	// Variable for player looking controls
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

private:
	// Player's starting stamina
	UPROPERTY(EditAnywhere, Category = "Stamina")
		float InitialStamina;

	// Player's current stamina
	UPROPERTY(EditAnywhere, Category = "Stamina")
		float CurrentStamina;

	// Inventory pickup tracking
	UPROPERTY(EditAnywhere, Category = "Inventory")
		int CollectibleItem = 0;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Inventory management
	UFUNCTION(BlueprintCallable)
		void AddCollectibleItem(int Value);
	UFUNCTION(BlueprintPure)
		int GetCollectibleItem();

	// Forward and backward movement
	void Forward(float Value);

	// Left and right movement
	void Right(float Value);

	// Sprinting capability
	void EnableSprint();

	// Stops sprinting
	void DisableSprint();

	// Checks to see if character is sprinting
	bool bIsSprinting = false;

	// Template for int32
	void ChangeView(int32 Cam);
	template<int32 cam>
	void ChangeView()
	{
		ChangeView(cam);
	}
};
