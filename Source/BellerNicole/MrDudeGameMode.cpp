// Fill out your copyright notice in the Description page of Project Settings.

#include "MrDudeGameMode.h"
#include "BellerNicole.h"
#include "MrDude.h"
#include "Blueprint/UserWidget.h"

void AMrDudeGameMode::BeginPlay()
{
	Super::BeginPlay();
	ChangeMenuWidget(StartingWidget);

	// Gets the player character
	AMrDude* MyCharacter = Cast<AMrDude>(UGameplayStatics::GetPlayerPawn(this, 0));
	
	if (PlayerHUDClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);

		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}

void AMrDudeGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	// Checks to see if current widget is set
	if (CurrentWidget != nullptr)
	{
		// True means removing from the viewport
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}

	// Makes sure parameter is being passed
	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);

		// Verifies last line was successful
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}