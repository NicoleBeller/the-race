/*
**		Section Two: Camera Systems.  This code allows the player to use the various cameras that are setup throughout the level.  There's also
**		a section of code in this program that allows the player to toggle from first-person point of view to third-person point of view.  This
**		way, the player can play the game the way they truly want to.  Also, using the various cameras can allow the player to see different 
**		views that they wouldn't be able to see traditionally, by only using the 'follow-camera'.  
**
*/

#include "CameraViews.h"
#include "Kismet/GameplayStatics.h"
#include "MrDude.h"

// Sets default values
ACameraViews::ACameraViews()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraViews::BeginPlay()
{
	Super::BeginPlay();
	
	// Set camera control to current player controller
	OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);
	PlayerCamera = OurPlayerController->GetViewTarget();
}

// Called every frame
void ACameraViews::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Sets Player Camera view
void ACameraViews::SetPlayerCam()
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != PlayerCamera)
		{
			OurPlayerController->SetViewTarget(PlayerCamera);
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("Set to Player View"));
		}
	}
}

// Sets to Corner Camera view
void ACameraViews::SetCornerCam()
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != CornerCamera)
		{
			OurPlayerController->SetViewTarget(CornerCamera);
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("Set to Corner View"));
		}
	}
}

// Sets to Far Away Camera view
void ACameraViews::SetTopDownCam()
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != TopDownCamera)
		{
			OurPlayerController->SetViewTarget(TopDownCamera);
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("Set to Far Away View (Downstairs)"));
		}
	}
}

// Sets to Corner Camera (Upstairs) View
void ACameraViews::SetCornerViewCamU()
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != CornerViewCameraU)
		{
			OurPlayerController->SetViewTarget(CornerViewCameraU);
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("Set to Corner View (Upstairs)"));
		}
	}
}

// Sets to Hallway Camera (Upstairs)
void ACameraViews::SetHallwayCam()
{
	if (OurPlayerController)
	{
		if (OurPlayerController->GetViewTarget() != HallwayCamera)
		{
			OurPlayerController->SetViewTarget(HallwayCamera);
			GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, TEXT("Set to Hallway View (Upstairs)"));
		}
	}
}