#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Runtime/Engine/Classes/Engine/TargetPoint.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "AIEnemies.generated.h"

/**
 * 
 */
UCLASS()
class BELLERNICOLE_API AAIEnemies : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;

	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

private:
	// Waypoints for AI to stop/hit
	UPROPERTY()
		TArray<AActor*> Waypoints;

	// Timer for AI
	FTimerHandle TimerHandle;

	// Random target points
	UFUNCTION()
		ATargetPoint* GetRandomWaypoint();

	// Random waypoints
	UFUNCTION()
		void GoToRandomWaypoint();

	// Goes to next waypoint
	void GoToNextWaypoint();

	// Keeps track of current waypoint
	int currentWPIndex = 0;

	// Keeps track of size of array
	int WPIndexCount = 0;

public:
	// To be added to the "Random AI" blueprint
	UPROPERTY(EditAnywhere)
		bool bIsPathRandom = false;

};
