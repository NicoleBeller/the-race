#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PawnCollision.generated.h"

UCLASS()
class BELLERNICOLE_API APawnCollision : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnCollision();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// PawnMover class reference
	class UPawnMover* OurMovementComponent;
	virtual UPawnMovementComponent *GetMovementComponent() const override;

	// PawnMover can move backward and forward
	void Forward(float Value);

	// PawnMover can move left and right
	void Right(float Value);

	// PawnMover can change paths
	void Turn(float Value);
	
};
